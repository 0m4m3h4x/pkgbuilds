#!/bin/bash

# Packages to build. Make sure that the dependency order is correct!
PACKAGES=(
    asuki
    miko-menu-bar
    miko-settings
    miko-wallpapers
    miko-background-daemon
    miko-session
)

LOGFILE=$(realpath `dirname $0`)/build.log
MAKEPKG_CONFIG="/etc/makepkg.conf"

show_usage() {
    echo "Usage: $0 [-h,--help] <stable,testing> [-i,--install] [-f,--failfast]"
    echo "    -h, --help:     Displays this message"
    echo "    -i, --install:  Installs the built packages"
    echo "    -f, --failfast: Stop the build on any error"
}

quiet() {
    "$@" > /dev/null 2>&1
}

msg() {
    echo "[$(date +'%d/%m/%Y %T')] ==> $@" | tee -a "$LOGFILE"
}

msg2() {
    echo "[$(date +'%d/%m/%Y %T')] --> $@" | tee -a "$LOGFILE"
}

if [ -e "makepkg.conf" ] ; then
    MAKEPKG_CONFIG=$(realpath `dirname $0`)/makepkg.conf
fi

MAKEPKG_ARGS=(
    -scC --noconfirm --config "$MAKEPKG_CONFIG"
)
FAIL_FAST=no

dir=$1

for i in "$@"; do
    case $i in
        -h|--help)
        show_usage
        exit
        ;;
        -i|--install)
        MAKEPKG_ARGS+=("-i")
        ;;
        -f|--failfast)
        FAIL_FAST=yes
        ;;
    esac
done

if [ ! -e $dir ] || [ "$dir" == "" ] ; then
    show_usage
    exit 1
fi

if [ -e "$LOGFILE" ] ; then
    mv "$LOGFILE"{,.old}
fi

msg Starting $dir build...
quiet pushd $dir

fail_fast() {
    msg2 Build failed.
    if [ "$FAIL_FAST" == "yes" ] ; then
        exit 1
    fi
}

for pkg in ${PACKAGES[@]}; do
    msg2 Building $pkg...
    quiet pushd $pkg
    makepkg "${MAKEPKG_ARGS[@]}" || fail_fast
    quiet popd
done

quiet popd
msg Build finished.
