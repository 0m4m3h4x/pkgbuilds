#!/bin/bash

YELLOW="\x1b[1;33m"
RESET="\x1b[0m"

if [ ! -e makepkg.conf ] ; then
    printf "    $YELLOW/!\\ WARNING /!\\ \n"
    echo "This script is intended to be used by Miko maintainers."
    echo "Unless you know what you're doing, you are not supposed to use this."
    printf "$RESET"
    exit 1
fi

REPOS=(testing)

for repo in "${REPOS[@]}"; do
    # Create necessary directories
    mkdir -p repos/$repo
    ln -s `realpath repos/$repo` /tmp/miko-pkgs
    # Build the repository
    ./build.sh $repo
    # Create the pacman database
    pushd repos/$repo > /dev/null
    repo-add miko-$repo.db.tar.gz *.pkg.tar.zst
    popd > /dev/null
    # Remove /tmp/miko-pkgs
    rm /tmp/miko-pkgs
done
